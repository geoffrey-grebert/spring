package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.*;

@SpringBootApplication
@RestController
public class Application {

	@Value("${data.key}")
	private String myKey;

    @RequestMapping("/")
    public String home() {
        return "Hello " + myKey + " - " + System.getenv("DOUDOU");
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
